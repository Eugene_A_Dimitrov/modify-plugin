package ut.com.mycompany.bitbucket.plugin;

import org.junit.Test;
import com.mycompany.bitbucket.plugin.api.MyPluginComponent;
import com.mycompany.bitbucket.plugin.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}