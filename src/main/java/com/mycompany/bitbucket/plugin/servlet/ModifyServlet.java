package com.mycompany.bitbucket.plugin.servlet;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;


public class ModifyServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(ModifyServlet.class);
    private final RepositoryService repositoryService;
    private final ApplicationPropertiesService applicationPropertiesService;

    public ModifyServlet(
            RepositoryService repositoryService,
            ApplicationPropertiesService applicationPropertiesService) {
        this.repositoryService = repositoryService;
        this.applicationPropertiesService = applicationPropertiesService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        String[] uriSplit = pathInfo.split("/");
        final Repository repository = repositoryService.getBySlug(uriSplit[1], uriSplit[2]);
        final File location = applicationPropertiesService.getRepositoryDir(repository);

        FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
        repositoryBuilder.setMustExist(true);
        repositoryBuilder.setGitDir(location);
        org.eclipse.jgit.lib.Repository repo = repositoryBuilder.build();


        ObjectInserter repoInserter = repo.newObjectInserter();

        // Add a blob to the repository
        CommitBuilder commitBuilder = new CommitBuilder();
        commitBuilder.setTreeId(repoInserter.insert(Constants.OBJ_TREE, new byte[] {}));
        PersonIdent ident = new PersonIdent("test", "test@domain.com");
        commitBuilder.setCommitter(ident);
        commitBuilder.setAuthor(commitBuilder.getCommitter());
        commitBuilder.setMessage("Empty commit");

        ObjectId commitId = repoInserter.insert(commitBuilder);
        repoInserter.flush();

        RefUpdate refUpdate = repo.updateRef("refs/heads/master");
        refUpdate.setNewObjectId(commitId);
        refUpdate.forceUpdate();

        try {
            resp.setContentType("text/html");
            resp.getWriter().write("<html><body>Commit Success! Commit id: " + commitId + "</body></html>");

        } catch (IOException e) {
            resp.setContentType("text/html");
            resp.getWriter().write("<html><body>Fail</body></html>");
        }
    }

}